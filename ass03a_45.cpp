#include "bits/stdc++.h"
#define BURST_MAX 20
#define BURST_MIN 1
#define LAMBDA 0.5
#define BATCH_SIZE 10
#define QUANT_TYPES 3
#define SAMPLE_TYPES 3
using namespace std;

vector <double> getUniformNumbers (int size, int start, int finish) {
    vector <double> numbers;
    int iter;
    unsigned seed = (unsigned int)chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator (seed);
    uniform_real_distribution <double> dist(start, finish);
    for (iter = 0; iter < size; iter ++)
        numbers.push_back(dist(generator));
    return numbers;
}

vector <int> genProcessArrT (int size) {
    vector <double> uTimes = getUniformNumbers(size, 0, 1);
    vector <int> eTimes;
    int iter;
    for (auto uni: uTimes) {
        eTimes.push_back((int)((-1.0 / LAMBDA) * log(uni)));
    }
    for (iter = 1; iter < size; iter ++)
        eTimes [iter] = eTimes [iter] + eTimes [iter - 1];
    return eTimes;
}

vector <int> genBurstT (int size) {
    vector <int> uTimes;
    vector <double> uTimesFlt = getUniformNumbers(size, BURST_MIN, BURST_MAX);
    for (auto ele: uTimesFlt)
        uTimes.push_back((int)(ele));
    return uTimes;
}

double fcfs (vector <int> processArrT, vector <int> burstT) {
    int currProcess, numProcess = (int) burstT.size();
    int  currTime = burstT[0], currWait, totWaitTime = 0;
    for (currProcess = 1; currProcess < numProcess; currProcess ++) {
        if ((currWait = currTime - processArrT[currProcess]) > 0) {
            totWaitTime += currWait;
        } else {
            currTime = processArrT[currProcess];
        }
        currTime += burstT[currProcess];
    }
    return ((totWaitTime * 1.0) / numProcess);
}

double sjfp (vector <int> processArrT, vector <int> burstT) {
    vector <int> remainingT (burstT.begin(), burstT.end());
    int complete, size = (int) burstT.size(), jobIter, currTime, currShortestJobT = INT_MAX, currShortestJob, totWaitTime = 0;
    for (complete = 0, currTime = 1; complete < size; currTime ++) {
        for (jobIter = 0, currShortestJob = -1; jobIter < size; jobIter ++ ) {
            if ((processArrT[jobIter] <= currTime) && (remainingT[jobIter] <= currShortestJobT) && (remainingT[jobIter] > 0)) {
                currShortestJobT = remainingT[jobIter];
                currShortestJob = jobIter;
            }
        }
        if (currShortestJob == -1)
            continue;
        currShortestJobT = -- remainingT[currShortestJob];
        if (currShortestJobT == 0) {
            complete ++;
            currShortestJobT = INT_MAX;
            totWaitTime += (currTime - processArrT[currShortestJob]) - burstT[currShortestJob];
        }
    }
    return ((totWaitTime * 1.0) / size);
}

double rr (vector <int> processArrT, vector <int> burstT, int quantum) {
    vector <int> remainingT (burstT.begin(), burstT.end());
    int jobIter, size = (int) burstT.size(), currTime = 0, incrTime, totWaitTime = 0;
    bool allDone = false;
    while (!allDone) {
        allDone = true;
        for (jobIter = 0; jobIter < size; jobIter ++) {
            if (remainingT [jobIter] == 0)
                continue;
            allDone = false;
            if (remainingT [jobIter] > quantum)
                incrTime = quantum;
            else {
                incrTime = remainingT [jobIter];
                totWaitTime += (currTime + remainingT [jobIter]) - processArrT [jobIter];
            }
            currTime += incrTime;
            remainingT [jobIter] -= incrTime;
        }
    }
    return ((totWaitTime * 1.0) / size);
}

int main() {
    int iter, arr[SAMPLE_TYPES] = {10, 50, 100}, quantum[QUANT_TYPES] = {1, 2, 5}, batch, cat, q, algo;
    double allData[SAMPLE_TYPES][2 + QUANT_TYPES][BATCH_SIZE], avg, data[SAMPLE_TYPES][2 + QUANT_TYPES];
    vector <int> processCount(arr, arr + sizeof(arr) / sizeof(arr[0]));
    vector <int> processArrT, burstT;
    vector <double> waitTimes;
    ofstream dataFile;
    for (iter = 0; iter < SAMPLE_TYPES; iter ++) {
        for (batch = 0; batch < BATCH_SIZE; batch ++) {
            processArrT = genProcessArrT(processCount[iter] - 1);
            processArrT.insert(processArrT.begin(), 0);
            burstT = genBurstT(processCount[iter]);
            allData [iter][0][batch] = fcfs(processArrT, burstT);
            allData [iter][1][batch] = sjfp(processArrT, burstT);
            for (q = 0; q < QUANT_TYPES; q++)
                allData[iter][q + 2][batch] = rr(processArrT, burstT, quantum[q]);
        }
    }
    for (iter = 0; iter < SAMPLE_TYPES; iter ++) {
        for (algo = 0; algo < 2 + QUANT_TYPES; algo ++) {
            avg = 0;
            for (batch = 0; batch < BATCH_SIZE; batch ++) {
                avg += allData[iter][algo][batch];
            }
            avg /= BATCH_SIZE;
            data[iter][algo] = avg;
        }
    }
    dataFile.open ("data.json");
    for (algo = 0; algo < 2 + QUANT_TYPES; algo++) {
        dataFile << "[ " << setw (5) << left << data[0][algo];
        for (iter = 1; iter < SAMPLE_TYPES; iter ++) {
            dataFile << "," << setw (5) << left << data[iter][algo];
        }
        dataFile << "],\n";
    }
    dataFile.close();
}